/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , sessions = require('./routes/sessions')
  , cons = require('consolidate')
  , swig = require('swig')
  , http = require('http')
  , path = require('path')
  , mongodb = require('mongodb');


var mongo = new mongodb.Db('nb_sessions',
    new mongodb.Server('localhost', 27017, {
        auto_reconnect: true
    })
);

mongo.open(function(err, db) {
    if(err) {
        console.log('Error connecting to mongodb');
    }
});

global.mongo = mongo;

var app = express();

// swig config
swig.init({
    root: __dirname + '/views',
    autoescape: true,
    cache: false,
    encoding: 'utf8'
});


// all environments
app.set('port', process.env.PORT || 3000);
app.engine('html', cons.swig);
app.set('views', __dirname + '/views');
app.set('view engine', 'html');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(require('less-middleware')({ src: __dirname + '/public' }));
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/sessions/available', sessions.available);
app.get('/sessions/past', sessions.past);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

mongo.close()
