exports.available = function(req, res){
    mongo.collection('sessions_available', function(err, collection) {
        collection.find().toArray(function(err, items) {
            var result = {
                sessions: items
            };
            if(!req.xhr) {
                res.render('sessions', result);
            } else {
                res.send(200, result);
            }
        });
    });
};

exports.past = function(req, res){
    res.render('sessions');
};
